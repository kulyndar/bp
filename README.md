# Webová aplikace pro vytvarný ateliér

Cílem této práce je provést analýzu a vytvořit návrh webové aplikace pro výtvarný ateliér. Aplikace bude sloužit komunikačním kanálem mezi ateliérem a zákazníky. Ateliér bude pravidelně vkládat nové návody jak se kreslí a maluje v podobě foto a video návodů. Kromě analýzy požadavků a prvotního návrhu bude provede porovnání a výběr vhodného nástroje pro vývoj aplikace.

## Přílohy

Přílohy k semestrálnímu projektu se nachází ve složce **appendix**. Jsou tam:

* Specifikace ve formátu ```.pdf```
* Katalog požadavků ve formátu ```.xlsx```
* Use Case diagramy ve složce UseCases ve formátu ```.pdf```
* Diagram procesů ve formátu ```.pdf```
* Doménový model ve formátu ```.pdf```
* Soubor projektu aplikace Enterprise Architect - ```Analyza.EAP```
* Wireframes ve složce wireframes:
  * jednotlivé obrazovky ve formátu ```.jpg```, rozdělené do složek podle verzi (poslední verze je 3)
  * projekt aplikace Adobe Illustrator - ```wireframe.ai```
* Harmonogram projektu ve formátu ```.pdf```

## Zpráva

Latexové soubory se zprávou se nachází ve složce ```project-document```.

